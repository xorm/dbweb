module gitea.com/xorm/dbweb

go 1.12

require (
	code.gitea.io/log v0.0.0-20190603145206-fc1de2249e88
	gitea.com/lunny/tango v0.6.0
	gitea.com/tango/binding v0.0.0-20190606022902-f7676e2641fd
	gitea.com/tango/captcha v0.0.0-20190606015415-a4920ba641a9
	gitea.com/tango/debug v0.0.0-20190529033538-c1361767ae1f
	gitea.com/tango/flash v0.0.0-20190606021323-2b17fd0aed7c
	gitea.com/tango/gitea_log_bridge v0.0.0-20190527042118-90d8909c0cbd
	gitea.com/tango/renders v0.0.0-20190606020507-5dfc5b32c70d
	gitea.com/tango/session v0.0.0-20190606020146-89f560e05167
	gitea.com/tango/xsrf v0.0.0-20190606015726-fb1b2fb84238
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
	github.com/Unknwon/i18n v0.0.0-20171114194641-b64d33658966
	github.com/denisenkom/go-mssqldb v0.0.0-20190515213511-eb9f6a1743f3
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-xorm/core v0.6.2
	github.com/go-xorm/xorm v0.7.1
	github.com/lib/pq v1.1.1
	github.com/lunny/log v0.0.0-20160921050905-7887c61bf0de // indirect
	github.com/lunny/nodb v0.0.0-20160621015157-fc1ef06ad4af
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/shurcooL/httpfs v0.0.0-20190527155220-6a4d4a70508b // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd
	github.com/siddontang/go-snappy v0.0.0-20140704025258-d8f7bb82a96d // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
	gopkg.in/ini.v1 v1.42.0 // indirect
)
