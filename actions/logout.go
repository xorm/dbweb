package actions

import (
	"gitea.com/xorm/dbweb/middlewares"

	"gitea.com/lunny/tango"
)

type Logout struct {
	RenderBase
	middlewares.AuthUser
	tango.Ctx
}

func (l *Logout) Get() {
	if l.IsLogin() {
		l.Logout()
	}
	l.Redirect("/")
}
