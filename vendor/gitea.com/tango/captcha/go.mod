module gitea.com/tango/captcha

go 1.12

require (
	gitea.com/lunny/tango v0.6.0
	gitea.com/tango/cache v0.0.0-20190326075255-36b7c0b3c0ed
	gitea.com/tango/renders v0.0.0-20190520025524-3d57bb0b4e29
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
	github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c // indirect
)
