package logbridge

import (
	"strings"

	"code.gitea.io/log"
	"gitea.com/lunny/tango"
)

var (
	formats           []string
	defaultFormatSize = 20
)

func genFormat(argsLen int) string {
	return strings.TrimSpace(strings.Repeat("%v ", argsLen))
}

func init() {
	formats = make([]string, defaultFormatSize, defaultFormatSize)
	for i := 0; i < defaultFormatSize; i++ {
		formats[i] = genFormat(i)
	}
}

// GiteaLogBridge a logger bridge from Logger to gitea log
type GiteaLogBridge struct {
	logger *log.Logger
}

// NewGiteaLogger inits a bridge for gitea log
func NewGiteaLogger(name string) tango.Logger {
	return &GiteaLogBridge{
		logger: log.GetLogger(name),
	}
}

// Log a message with defined skip and at logging level
func (l *GiteaLogBridge) Log(skip int, level log.Level, format string, v ...interface{}) error {
	return l.logger.Log(skip+1, level, format, v...)
}

// Debug show debug log
func (l *GiteaLogBridge) Debug(v ...interface{}) {
	l.Log(2, log.DEBUG, formats[len(v)], v...)
}

// Debugf show debug log
func (l *GiteaLogBridge) Debugf(format string, v ...interface{}) {
	l.Log(2, log.DEBUG, format, v...)
}

// Error show error log
func (l *GiteaLogBridge) Error(v ...interface{}) {
	l.Log(2, log.ERROR, formats[len(v)], v...)
}

// Errorf show error log
func (l *GiteaLogBridge) Errorf(format string, v ...interface{}) {
	l.Log(2, log.ERROR, format, v...)
}

// Info show information level log
func (l *GiteaLogBridge) Info(v ...interface{}) {
	l.Log(2, log.INFO, formats[len(v)], v...)
}

// Infof show information level log
func (l *GiteaLogBridge) Infof(format string, v ...interface{}) {
	l.Log(2, log.INFO, format, v...)
}

// Warn show warning log
func (l *GiteaLogBridge) Warn(v ...interface{}) {
	l.Log(2, log.WARN, formats[len(v)], v...)
}

// Warnf show warnning log
func (l *GiteaLogBridge) Warnf(format string, v ...interface{}) {
	l.Log(2, log.WARN, format, v...)
}
