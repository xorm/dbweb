module gitea.com/tango/binding

go 1.12

require (
	gitea.com/lunny/tango v0.6.0
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
)
