###################################
#Build stage
FROM golang:1.12-alpine AS build-env

#Build deps
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apk --no-cache add git
COPY ./ /extract
WORKDIR /extract
RUN go build -mod=vendor -tags 'bindata'

FROM alpine:3.9
LABEL maintainer="xiaolunwen@gmail.com"

EXPOSE 8090

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apk --no-cache add \
    bash \
    ca-certificates \
    curl \
    gettext \
    s6 \
    su-exec \
    tzdata

VOLUME ["/data"]

ENTRYPOINT ["/app/extract/extract"]

COPY --from=build-env ./extract/extract /app/extract/extract